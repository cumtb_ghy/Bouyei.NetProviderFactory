﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Bouyei.NetFactory.Udp
{
    public class UdpServerProvider : IDisposable
    {
        #region variable
        private SocketReceive socketRecieve = null;
        private SocketSend socketSend = null;
        private bool _isDisposed = false;
        private Encoding encoding = Encoding.UTF8;

        #endregion

        #region property

        public OnReceivedOffsetHandler ReceiveOffsetHanlder { get; set; }

        /// <summary>
        /// 接收事件响应回调
        /// </summary>
        public OnReceivedHandler ReceiveCallbackHandler { get; set; }

        /// <summary>
        /// 发送事件响应回调
        /// </summary>
        public OnSentHandler SentCallbackHandler { get; set; }

        /// <summary>
        /// 断开连接事件回调
        /// </summary>
        public OnDisconnectedHandler DisconnectedCallbackHandler { get; set; }

        #endregion

        #region structure
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (_isDisposed) return;

            if (isDisposing)
            {
                socketRecieve.Dispose();
                socketSend.Dispose();
                _isDisposed = true;
            }
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        public UdpServerProvider()
        {

        }

        /// <summary>
        /// 启动服务
        /// </summary>
        /// <param name="port">接收数据端口</param>
        /// <param name="recBufferSize">接收缓冲区</param>
        /// <param name="maxConnectionCount">最大客户端连接数</param>
        public void Start(int port,
            int recBufferSize,
            int maxConnectionCount)
        {
            socketSend = new SocketSend(maxConnectionCount, recBufferSize);
            socketSend.SentEventHandler += sendSocket_SentEventHandler;

            socketRecieve = new SocketReceive(port,maxConnectionCount, recBufferSize);
            socketRecieve.OnReceived += receiveSocket_OnReceived;
            socketRecieve.StartReceive();
        }
        #endregion

        #region public method
        /// <summary>
        /// 停止服务
        /// </summary>
        public void Stop()
        {
            if (socketSend != null)
            {
                socketSend.Dispose();
            }
            if (socketRecieve != null)
            {
                socketRecieve.StopReceive();
            }
        }
 
        public bool Send(SegmentOffset dataSegment,IPEndPoint remoteEP ,bool waiting = true)
        {
            return socketSend.Send(dataSegment, remoteEP, waiting);
        }
 
        public int SendSync(IPEndPoint remoteEP, SegmentOffset dataSegment)
        {
            return socketSend.SendSync(dataSegment , remoteEP);
        }
        #endregion

        #region private method
        private void sendSocket_SentEventHandler(object sender, SocketAsyncEventArgs e)
        {
            if (SentCallbackHandler != null && isServerResponse(e) == false)
            {
                SentCallbackHandler(new SegmentToken(new SocketToken()
                {
                    TokenIpEndPoint = (IPEndPoint)e.RemoteEndPoint
                }, e.Buffer, e.Offset, e.BytesTransferred));
            }
        }

        private void receiveSocket_OnReceived(object sender, SocketAsyncEventArgs e)
        {
            if (isClientRequest(e)) return;

            SocketToken sToken = new SocketToken()
            {
                TokenIpEndPoint = (IPEndPoint)e.RemoteEndPoint
            };

            if (ReceiveOffsetHanlder != null)
                ReceiveOffsetHanlder(new SegmentToken(sToken, e.Buffer, e.Offset, e.BytesTransferred));

            if (ReceiveCallbackHandler != null)
            {
                if (e.BytesTransferred > 0)
                {
                    ReceiveCallbackHandler(sToken, encoding.GetString(e.Buffer, e.Offset, e.BytesTransferred));
                }
            }
        }

        private bool isClientRequest(SocketAsyncEventArgs e)
        {
            if (e.BytesTransferred == 1 && e.Buffer[0] == 0)
            {
                socketSend.Send(new SegmentOffset(new byte[] { 1 }),
                  (IPEndPoint)e.RemoteEndPoint, true);
                return true;
            }
            else return false;
        }

        private bool isServerResponse(SocketAsyncEventArgs e)
        {
            if (e.BytesTransferred == 1 && e.Buffer[0] == 1)
            {
                return true;
            }
            else return false;
        }

        #endregion
    }
}